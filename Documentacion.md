# Tienda de stickers

## Caratula

### Integrantes

   Mauricio Alejandro Nampula Martinez  
   Amairany  
   Juilo Hernandez Lorenzo  

### Materia
    
   Programaciòn Multimedia

## Indice

- [Tienda de stickers](#tienda-de-stickers)
    - [Caratula](#caratula)
        - [Integrantes](#integrantes)
        - [Materia](#materia)
    - [Indice](#indice)
    - [Introducciòn](#introducci%C3%B2n)
    - [Propuesta](#propuesta)
        - [¿Qué es?](#%C2%BFqu%C3%A9-es)
        - [¿Por qué?](#%C2%BFpor-qu%C3%A9)
    - [Investigación](#investigaci%C3%B3n)
        - [Cliente](#cliente)
        - [Tipo de cliente](#tipo-de-cliente)
    - [Información](#informaci%C3%B3n)
    - [Contenidos](#contenidos)
        - [Servidor](#servidor)
            - [Requisitos previos](#requisitos-previos)
            - [Instalación](#instalaci%C3%B3n)
    - [Conclusiones](#conclusiones)
    - [Anexos](#anexos)
    - [Referencias bibliograficas](#referencias-bibliograficas)

## Introducciòn

   El equipo realizo la implementaciòn, configuraciòn y despliegue de una tienda de ecomerce para stickers, nos basamos en una soluciòn preconstruida, contrario a lo que pueda creerse, el despliegue no es sencillo, entran en juego muchas variables que no son contempladas a primera vista, las versiones de PHP, el servidor donde estara montado, la logistica del servicio a cliente, la administraciòn del catalogo, el curso de inducción para los administradores de la tienda.
   
   Este documento tiene la finalidad de proveer los elementos necesarios para agilizar el proceso de aprendizaje y responder dudas que puedan llegar a surgir en el curso de inducción.
## Propuesta

### ¿Qué es?	

El proyecto a presentar es una tienda virtual, donde se puedan mostrar diferentes diseños de _sticker’s_, hacer pedidos en diferentes cantidades, cotizar los precios, tener un contacto directo con el diseñador para hacer modificaciones en algún diseño o bien solicitar que se haga un sticker personalizado. 

### ¿Por qué?

Como se sabe los _sticker’s_ son dibujos que pueden personalizar el área de trabajo, los artículos personales (pc, móviles, parabrisas, muebles, etc.), o simplemente ser un regalo; por ello se considera que por el precio accesible para todo el público en general puede ser una manera de plasmar o sentirse identificado por el diseño seleccionado. 

## Investigación

### Cliente 

Nicho 
-	Clientes con gusto por caricaturas o dibujos y letras personalizados
-	Diseñadores 

### Tipo de cliente

Los sticker’s están diseñados para todo el público en general, ya que cada diseño puede ser representativo a la edad o caricatura favorita de cada uno, o bien, la atracción que pueda haber por la forma en que están elaborados. 

## Información 

Los clientes tienen un gran interés por dibujos con muchos colores, tamaños y formas. 



## Contenidos

El montaje del servicio se llevo acabo en un equipo propio, mantenido por **ISP** comercial con un sistema operativo **linux**, para ello se llevo acabo las siguientes acciones:

### Servidor

El servidor es el encargado de publicar la tienda virtual hacia internet, es necesario contar con el software requerido por **prestashop**, a continuacion se lista dichos requerimientos:

- Debian 9
- PHP 7.0+
- Mariadb 5.5+
- Apache 2.2+ 
- openssl  
> Lista completa de requerimientos en el anexo 1 

#### Requisitos previos

Es necesario crear una base de datos, asi como un usuario  y la asignación de permisos.

**Base de datos**

>`CREATE DATABASE tienda_sticker;`

**Usuario**

>`CREATE USER 'tienda'@'localhost' IDENTIFIED BY 'ContraseñaTienda';`

**Permisos**

>`GRANT ALL ON tienda_sticker.* TO 'tienda'@'localhost' IDENTIFIED BY 'ContraseñaTienda';`

**Apache**
Es necesario habilitar el mod_rewrite de apache, para poder configurar las URL *amigables* de **prestashop**.

>`ae2mod mod_rewrite`

>`systemctl restart apache2`

#### Instalación

Extraer el archivo zip descargado de la pagina de **prestashop**

> `unzip prestashop_XXX.zip /var/www/html/tienda`



## Conclusiones

## Anexos

- PHP 7.0+
    - php-common
    - mod-php
    - php-json
    - php-xml
    - php-gd
    - php-pdo
    - php-zip
    - php-curl
    - php-opcache
- Mariadb 5.5+
- Apache 2.2+ 
    - mod_ssl
    - mod_rewrite
- openssl
    - certificado de auntenticidad del dominio
    - ca-certs

## Referencias bibliograficas
